export interface IAuthor {
  _id: string;
  userId: string;
  bio?: string;
  authorName?: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface IEnrichedAuthor extends IAuthor {
  firstName: string;
  lastName: string;
  avatar: string;
}
