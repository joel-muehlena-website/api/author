import { model, Schema, Types as mongoTypes } from "mongoose";

const AuthorSchema = new Schema({
  userId: {
    type: mongoTypes.ObjectId,
    required: true,
  },
  authorName: {
    type: String,
    required: false,
  },
  bio: {
    type: String,
    required: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
  },
});

export const Author = model("author", AuthorSchema, "authors");
