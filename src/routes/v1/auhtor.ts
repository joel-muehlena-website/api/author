import { Router } from "express";
import { createAuthor } from "../../common/v1/createAuthor";
import { deleteAuthor } from "../../common/v1/deleteAuthor";
import { editAuthor } from "../../common/v1/editAuthor";
import { getAllAuthors, getAuthorById } from "../../common/v1/getAuthorEntries";

export const authorV1Router = Router();

authorV1Router.get("/author", getAllAuthors);
authorV1Router.get("/author/:id", getAuthorById);
authorV1Router.post("/author", createAuthor);
authorV1Router.patch("/author/:id", editAuthor);
authorV1Router.delete("/author/:id", deleteAuthor);
