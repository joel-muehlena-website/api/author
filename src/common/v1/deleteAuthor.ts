import { NextFunction, Request } from "express";
import { Author } from "../../models/v1/author";
import { IAuthor } from "../../models/v1/author.interface";
import { ApiResponse } from "./ApiResponse";

export const deleteAuthor = async (
  req: Request,
  res: ApiResponse,
  next: NextFunction
) => {
  const { id } = req.params;

  const author: IAuthor | undefined = await Author.findByIdAndDelete(id);

  if (!author) {
    res.status(404).json({ code: 404, msg: "No author with this id found" });
    return next();
  }

  res.json({ code: 200, msg: "Success", data: author });
  return next();
};
