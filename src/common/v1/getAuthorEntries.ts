import { NextFunction, Request } from "express";
import axios from "axios";

import { Author } from "../../models/v1/author";
import { IAuthor, IEnrichedAuthor } from "../../models/v1/author.interface";
import { IUser } from "../../models/v1/user.interface";
import { ApiResponse } from "./ApiResponse";

const USER_API_URL =
  process.env.NODE_ENV === "production"
    ? "http://user.jm-api.svc.cluster.local"
    : "http://localhost:3011";

export const getAllAuthors = async (
  _: Request,
  res: ApiResponse,
  next: NextFunction
) => {
  const authors: IAuthor[] = await Author.find();

  if (authors.length == 0) {
    res.status(404).json({ code: 404, msg: "No authors found" });
    return next();
  }

  const allUserIds: string[] = [];

  for (let author of authors) {
    allUserIds.push(author.userId);
  }

  try {
    const response = await axios.post(`${USER_API_URL}/v1/user/requestList`, {
      requestedUsers: allUserIds,
    });
    const enrichedAuthors: IEnrichedAuthor[] = [];

    for (let user of response.data.data as IUser[]) {
      const author = authors.find((author) => author.userId == user._id);

      if (author) {
        const eAuthor: IEnrichedAuthor = {
          _id: author._id,
          userId: author.userId,
          bio: author.bio,
          authorName: author.authorName,
          createdAt: author.createdAt,
          updatedAt: author.updatedAt,
          avatar: user.avatar,
          firstName: user.firstName,
          lastName: user.lastName,
        };

        console.log(eAuthor);

        enrichedAuthors.push(eAuthor);
      }
    }

    res.status(200).json({ code: 200, msg: "Success", data: enrichedAuthors });
    return next();
  } catch (err) {
    res.status(500).json({
      code: 500,
      msg: "There was an error fetching the user data for the authors",
      error: err,
    });
    throw new Error("Error fetching the user data from user service: " + err);
  }
};

export const getAuthorById = async (
  req: Request,
  res: ApiResponse,
  next: NextFunction
) => {
  const { id } = req.params;

  const author: IAuthor | undefined = await Author.findById(id);
  if (!author) {
    res.status(404).json({ code: 404, msg: "No author with this id found" });
    return next();
  }

  try {
    const response = await axios.get(
      `${USER_API_URL}/v1/user/${author.userId}`
    );

    const user: IUser = response.data.data;

    const eAuthor: IEnrichedAuthor = {
      _id: author._id,
      userId: author.userId,
      bio: author.bio,
      authorName: author.authorName,
      createdAt: author.createdAt,
      updatedAt: author.updatedAt,
      firstName: user.firstName,
      lastName: user.lastName,
      avatar: user.avatar,
    };

    res.status(200).json({ code: 200, msg: "Success", data: eAuthor });
    return next();
  } catch (err) {
    res.status(500).json({
      code: 500,
      msg: "There was an error fetching the user data for the author",
      error: err,
    });
    throw new Error("Error fetching the user data from user service: " + err);
  }
};
