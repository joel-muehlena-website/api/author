import { Response } from "express";

export type ApiResponse = Response<
  { code: number; msg: string; data?: any; error?: any },
  any
>;
