import { NextFunction, Request } from "express";
import { Author } from "../../models/v1/author";
import { IAuthor } from "../../models/v1/author.interface";
import { ApiResponse } from "./ApiResponse";

export const editAuthor = async (
  req: Request,
  res: ApiResponse,
  next: NextFunction
) => {
  const { id } = req.params;

  const author: IAuthor = await Author.findById(id);

  if (!author) {
    res.status(404).json({
      code: 404,
      msg: `No author entry with the id ${id} found. Could not be edited.`,
    });

    return next();
  }

  const { authorName, bio } = req.body;

  const updatedAuthor: Partial<IAuthor> = {};
  if (authorName) updatedAuthor.authorName = authorName;
  if (bio) updatedAuthor.bio = bio;
  updatedAuthor.updatedAt = new Date();

  const updatedData = await Author.findByIdAndUpdate(
    id,
    { $set: updatedAuthor },
    { new: true }
  );

  if (updatedData) {
    res.status(200).json({ code: 200, msg: "Success", data: updatedData });

    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to update the author entry" });

    return next();
  }
};
