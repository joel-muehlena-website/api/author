import { NextFunction, Request } from "express";
import { Author } from "../../models/v1/author";
import { IAuthor } from "../../models/v1/author.interface";
import { ApiResponse } from "./ApiResponse";

export const createAuthor = async (
  req: Request,
  res: ApiResponse,
  next: NextFunction
) => {
  const { userId, bio, authorName } = req.body;

  const currDate = new Date();
  const author: Omit<IAuthor, "_id"> = {
    userId,
    bio,
    authorName,
    createdAt: currDate,
    updatedAt: currDate,
  };

  const newAuthor = await new Author(author).save();

  if (newAuthor) {
    res.status(200).json({ code: 200, msg: "Success", data: newAuthor });

    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to create the author entry" });

    return next();
  }
};
